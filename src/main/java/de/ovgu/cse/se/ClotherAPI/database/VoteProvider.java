package de.ovgu.cse.se.ClotherAPI.database;

import java.util.List;

import com.dreamfactory.client.ApiException;

import de.ovgu.cse.se.ClotherAPI.models.User;
import de.ovgu.cse.se.ClotherAPI.models.Vote;

public class VoteProvider {
	public final String tableName = "votes";

	public VoteProvider() {

	}

	public Vote addVote(Vote vote) throws ApiException {
		return SessionProvider.getSession().addModel(vote, tableName);
	}

	public Vote deleteVote(Vote vote) throws ApiException {
		return SessionProvider.getSession().delModel(vote, tableName,
				vote.getId().toString());
	}

	public Vote updateVote(Vote vote) throws ApiException {
		return SessionProvider.getSession().updModel(vote, tableName,
				vote.getId().toString());
	}

	public Vote getVote(Long voteId) throws ApiException {
		return SessionProvider.getSession().getModel(Vote.class, tableName,
				voteId.toString());
	}

	public List<Vote> getOwnVotes(User user) throws ApiException {
		String filter = String.format("CREATOR = '%d'", user.getId());
		List<Vote> votes = SessionProvider.getSession().findModel(Vote.class,
				tableName, filter);
		return votes;
	}

}
