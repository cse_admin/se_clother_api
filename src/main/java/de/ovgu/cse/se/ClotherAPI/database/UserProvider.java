package de.ovgu.cse.se.ClotherAPI.database;

import com.dreamfactory.client.ApiException;
import de.ovgu.cse.se.ClotherAPI.models.User;

import java.util.List;

public class UserProvider {
    public final String tableName = "users";

    public UserProvider() {
    }


    public User addUser(User user) throws ApiException {
        return SessionProvider.getSession().addModel(user, tableName);
    }


    public User deleteUser(User user) throws ApiException {
        return SessionProvider.getSession().delModel(user, tableName, user.getId().toString());
    }


    public User updateUser(User user) throws ApiException {
        return SessionProvider.getSession().updModel(user, tableName, user.getId().toString());
    }


    public List<User> getUser() throws ApiException {
        return SessionProvider.getSession().listModel(User.class, tableName);
    }


	public User getUser(String userEmail, String userPassword) throws ApiException{
        assert ! userEmail.contains("'");
        assert ! userPassword.contains("'");
        String filter = String.format("EMAIL = '%s' AND PASSWORD = '%s'", userEmail, userPassword);
        List<User> user = SessionProvider.getSession().findModel(User.class, tableName, filter);
        if (user.size() == 1) {
            return user.get(0);
        } else {
            throw new ApiException(417, "ZeroOrManyFound");
        }
	}

}
