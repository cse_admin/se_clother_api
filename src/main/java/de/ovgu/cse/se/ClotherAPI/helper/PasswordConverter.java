package de.ovgu.cse.se.ClotherAPI.helper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordConverter {
	private static final byte[] SALT = "huYgoHf3aFrnDD9Q".getBytes();

	public String convertToSHA(String ccNumber) {
		// do some encryption
		String generatedPassword = null;

		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-512");
			md.update(SALT);
			byte[] bytes = md.digest(ccNumber.getBytes());
			StringBuilder sb = new StringBuilder();
			for (byte aByte : bytes) {
				sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}
}
