package de.ovgu.cse.se.ClotherAPI.database;

import com.dreamfactory.client.ApiException;
import de.ovgu.cse.se.ClotherAPI.models.Tag;

import java.util.List;

public class TagProvider {
	public final String tableName = "tags";

	public TagProvider() {

	}

	public Tag addTag(Tag tag) throws ApiException {
		return SessionProvider.getSession().addModel(tag, tableName);
	}

	public Tag deleteTag(Tag tag) throws ApiException {
		return SessionProvider.getSession().delModel(tag, tableName, tag.getId().toString());
	}

	public Tag updateTag(Tag tag) throws ApiException {
		return SessionProvider.getSession().updModel(tag, tableName, tag.getId().toString());
	}

	public Tag getTag(Long tagId) throws ApiException {
		return SessionProvider.getSession().getModel(Tag.class, tableName, tagId.toString());
	}

	public Tag getTag(String tagName) throws ApiException {
		assert ! tagName.contains("'");
		String filter = String.format("NAME = '%s'", tagName);
		List<Tag> tags = SessionProvider.getSession().findModel(Tag.class, tableName, filter);
		if (tags.size() == 1) {
			return tags.get(0);
		} else {
			throw new ApiException(417, "ZeroOrManyFound");
		}
	}

	public List<Tag> getTags() throws ApiException{
		return SessionProvider.getSession().listModel(Tag.class, tableName);
	}

}
