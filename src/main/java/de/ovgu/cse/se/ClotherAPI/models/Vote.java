package de.ovgu.cse.se.ClotherAPI.models;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(scope=Vote.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "VOTE_ID")
public class Vote implements IEntity {

	@JsonProperty("VOTE_ID")
	private Long id;
	//@JsonProperty("PICTURE_ID")
	@JsonBackReference(value="vote-picture")
	private Picture picture;
	//@JsonProperty("USER_ID")
	@JsonBackReference(value="vote-user")
	private User creator;
	@JsonProperty("RATING")
	private int rating;
	@JsonProperty("COMMENT")
	private String comment;
	@JsonProperty("CREATION_TIME")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date creationTime;
	
	public Long getId() {
		return id;
	}

	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public Date getCreationTime() {
		return creationTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
}
