package de.ovgu.cse.se.ClotherAPI;

import de.ovgu.cse.se.ClotherAPI.exceptions.IllegalObjectProviderContextException;

public class ObjectProviderFactory {

	private static IObjectProvider testProvider;
	private static IObjectProvider prodProvider;
	private static IObjectProvider mockProvider;

	private static ConfigurationContext CurrentContext;

	public static IObjectProvider getObjectProvider(ConfigurationContext context) {
		try {
			if (context == ConfigurationContext.PRODUCTION) {
				if (prodProvider == null)
					CurrentContext = context;
				prodProvider = new ObjectProvider(context);
				return prodProvider;
			}

			if (context == ConfigurationContext.TEST) {
				CurrentContext = context;
				if (testProvider == null)
					testProvider = new ObjectProvider(context);
				return testProvider;
			}
			
			if (context == ConfigurationContext.MOCKUP) {
				if (mockProvider == null)
					CurrentContext = context;
				mockProvider = new ObjectProviderMockUp(context);
				return mockProvider;
			}
		} catch (IllegalObjectProviderContextException e) {
			// everything is fine.. null is returned
		}
		return null;
	}

	public static IObjectProvider getObjectProvider() {
		if (CurrentContext == null)
			CurrentContext = ConfigurationContext.PRODUCTION;
		return getObjectProvider(CurrentContext);
	}
}
