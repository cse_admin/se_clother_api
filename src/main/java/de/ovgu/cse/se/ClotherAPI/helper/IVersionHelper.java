package de.ovgu.cse.se.ClotherAPI.helper;

import de.ovgu.cse.se.ClotherAPI.exceptions.VersionCouldNotbeResolvedException;

import java.io.IOException;
import java.io.InputStream;

public interface IVersionHelper {

	String getApiVersion(InputStream stream)
			throws VersionCouldNotbeResolvedException;

	boolean isApiUpToDate() throws VersionCouldNotbeResolvedException;

	String getOnlineVersion() throws VersionCouldNotbeResolvedException, IOException;

	String getLocalVersion() throws VersionCouldNotbeResolvedException, IOException;
}