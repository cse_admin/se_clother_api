package de.ovgu.cse.se.ClotherAPI.models;

/**
 * Created by Severin Orth on 18.05.15.
 * This file is part of the ClotherAPI project
 */
public interface IEntity {
    Long getId();
}
