package de.ovgu.cse.se.ClotherAPI.exceptions;

import com.dreamfactory.client.ApiException;

/**
 * Created by Severin Orth on 18.05.15.
 * This file is part of the ClotherAPI project
 */
public class UnknownColumnName extends ApiException {
    private final String caption;


    public UnknownColumnName(String caption, String message) {
        super();

        this.caption = caption;

        this.setMessage(message);
    }


    @Override public String getMessage() {
        return this.caption + System.lineSeparator() + super.getMessage();
    }
}
