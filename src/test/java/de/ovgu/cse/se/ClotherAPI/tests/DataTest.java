package de.ovgu.cse.se.ClotherAPI.tests;

import de.ovgu.cse.se.ClotherAPI.exceptions.NotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.NotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.models.Picture;
import de.ovgu.cse.se.ClotherAPI.models.Tag;
import de.ovgu.cse.se.ClotherAPI.models.Type;
import de.ovgu.cse.se.ClotherAPI.models.User;
import de.ovgu.cse.se.ClotherAPI.models.Vote;

public class DataTest extends AbstractTest {
	//@Test
	public void createTestData() throws NotAddedException, UserdataNotCorrectException, NotUpdatedException, UserNotAuthenticatedException {

		authenticate();
		
		for (int i = 0; i < 100; i++) {
			User user = generator.generateNewUser();
			provider.addUser(user);
			
			Picture picture = generator.generateNewPicture();
			picture.setCreator(user);
			provider.addPicture(picture);
			
			for (int j = 0; i < 5; i++) {
				Tag tag = generator.generateNewTag();
				Type type = generator.generateNewType();
				provider.addType(type);
				tag.setType(type);
				picture.addTag(tag);
				provider.addTag(tag);
			}
			for (int j = 0; i < 5; i++) {
				Tag tag = generator.getRandomTag();
				tag.setType(generator.getRandomType());
				picture.addTag(tag);
				provider.addTag(tag);
			}
			provider.updatePicture(picture);
			
			for (int j = 0; j < 10; j++) {
				Vote vote = generator.generateNewVote();
				provider.addVote(vote);
			}
			provider.updateUser();
		}
	}
}
