package de.ovgu.cse.se.ClotherAPI.tests;

import java.util.logging.Logger;

import org.junit.AfterClass;

import de.ovgu.cse.se.ClotherAPI.ConfigurationContext;
import de.ovgu.cse.se.ClotherAPI.IObjectProvider;
import de.ovgu.cse.se.ClotherAPI.ObjectProviderFactory;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.helper.EntityGenerator;
import de.ovgu.cse.se.ClotherAPI.models.User;

public abstract class AbstractTest {

	public IObjectProvider provider;
	public EntityGenerator generator;

	public User user;

	public AbstractTest() {
		provider = ObjectProviderFactory
				.getObjectProvider(ConfigurationContext.TEST);
		generator = new EntityGenerator();
	}

	public void authenticate() throws UserdataNotCorrectException {
		if (provider.getUser() == null) {
			Logger.getLogger("de.ovgu.cse.se.clotherapi").info(
					"Not authenticated yet. creating test user and logging in");
			String password = "password";
			User user = generator.getLastUser();
			user.setPassword(password);
			try {
				provider.addUser(user);
			} catch (UserNotAddedException e) {
				// User exists already. Everything is ok.
			}
			this.user = provider.authenticate(user.getEmail(), password);
		}
	}

	@AfterClass
	public static void after() {
		ObjectProviderFactory.getObjectProvider().closeConnection();
	}
}
