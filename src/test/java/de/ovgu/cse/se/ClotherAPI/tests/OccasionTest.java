package de.ovgu.cse.se.ClotherAPI.tests;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.models.Occasion;

public class OccasionTest extends AbstractTest {

	@Test
	public void createOccasionTest() throws OccasionNotAddedException, UserNotAuthenticatedException, UserNotAddedException, UserdataNotCorrectException {
		Occasion occasion = generator.generateNewOccasion();
		authenticate();
		provider.addOccasion(occasion);
	}

	@Test(expected = OccasionNotAddedException.class)
	public void noEqualOccasionnameTest() throws OccasionNotAddedException, UserNotAuthenticatedException, UserNotAddedException, UserdataNotCorrectException {
		authenticate();
		String name = "test";
		
		Occasion occasion = new Occasion();
		occasion.setName(name);
		Occasion occasion2 = new Occasion();
		occasion2.setName(name);

		provider.addOccasion(occasion);
		provider.addOccasion(occasion2);
		// expect exception here
	}
	
	@Test
	public void getOccasionTest() throws UserdataNotCorrectException, OccasionNotAddedException, UserNotAuthenticatedException, UserNotAddedException, OccasionNotFoundException {
		authenticate();
		Occasion occasion = generator.generateNewOccasion();
		provider.addOccasion(occasion);
		
		occasion = provider.getOccasion(occasion.getName());
		Assert.assertNotNull(occasion);
	}
	
	@Test
	public void getOccasionsTest() throws UserdataNotCorrectException, OccasionNotAddedException, UserNotAuthenticatedException, UserNotAddedException, OccasionNotFoundException {
		authenticate();	
		List<Occasion> occasions = provider.getOccasions();
		Assert.assertNotNull(occasions);
	}
	
	@Test
	public void updateOccasionTest() throws OccasionNotUpdatedException,
			UserdataNotCorrectException, OccasionNotAddedException, UserNotAuthenticatedException, UserNotAddedException, OccasionNotFoundException {
		authenticate();
		Occasion occasion = generator.generateNewOccasion();
		provider.addOccasion(occasion);
		
		occasion = provider.getOccasion(occasion.getName());
		occasion.setName(occasion.getName() + "updated");
		provider.updateOccasion(occasion);
	}
}
