package de.ovgu.cse.se.ClotherAPI.tests;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.models.User;

public class AuthentificationTest extends AbstractTest{

	@Test
	public void authenticateUserTest() throws UserNotAddedException,
			UserdataNotCorrectException {
		User user = generator.generateNewUser();

		provider.addUser(user);

		User authenticatedUser = provider.authenticate(user.getEmail(),
				user.getPassword());

		Assert.assertEquals(
				"The authenticated user does not match the created user", user,
				authenticatedUser);
	}
	
	//based on issue #10
	@Test
	public void loginTwiceTest() throws UserNotAddedException, UserdataNotCorrectException {
		User user = generator.generateNewUser();
		String password = "password";
		user.setPassword(password);
		provider.addUser(user);
		provider.authenticate(user.getEmail(), password);
		Date date1 = provider.getUser().getLastLoginTime();
		provider.closeConnection();
		provider.authenticate(user.getEmail(), password);
		Date date2 = provider.getUser().getLastLoginTime();
		Assert.assertNotEquals("last login time was not updated correctly", date1, date2);
	}
}
