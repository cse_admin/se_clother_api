package de.ovgu.cse.se.ClotherAPI.tests;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.models.Tag;

public class TagTest extends AbstractTest {

	@Test
	public void createTagTest() throws TagNotAddedException, UserNotAuthenticatedException, UserNotAddedException, UserdataNotCorrectException {
		Tag tag = generator.generateNewTag();
		authenticate();
		provider.addTag(tag);
	}

	@Test(expected = TagNotAddedException.class)
	public void noEqualTagnameTest() throws TagNotAddedException, UserNotAuthenticatedException, UserNotAddedException, UserdataNotCorrectException {
		authenticate();
		String name = "test";
		
		Tag tag = new Tag();
		tag.setName(name);
		Tag tag2 = new Tag();
		tag2.setName(name);

		provider.addTag(tag);
		provider.addTag(tag2);
		// expect exception here

	}
	
	@Test
	public void getTagTest() throws UserdataNotCorrectException, TagNotAddedException, UserNotAuthenticatedException, UserNotAddedException, TagNotFoundException {
		authenticate();
		Tag tag = generator.generateNewTag();
		provider.addTag(tag);
		
		tag = provider.getTag(tag.getName());
		Assert.assertNotNull(tag);
	}
	
	@Test
	public void getTagsTest() throws UserdataNotCorrectException, TagNotAddedException, UserNotAuthenticatedException, UserNotAddedException, TagNotFoundException {
		authenticate();	
		List<Tag> tags = provider.getTags();
		Assert.assertNotNull(tags);
	}
	
	@Test
	public void updateTagTest() throws TagNotUpdatedException,
			UserdataNotCorrectException, TagNotAddedException, UserNotAuthenticatedException, UserNotAddedException, TagNotFoundException {
		authenticate();
		Tag tag = generator.generateNewTag();
		provider.addTag(tag);
		
		tag = provider.getTag(tag.getName());
		tag.setName(tag.getName() + "updated");
		provider.updateTag(tag);
	}
}
