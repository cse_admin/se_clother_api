package de.ovgu.cse.se.ClotherAPI.tests;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.models.Type;

public class TypeTest extends AbstractTest {

	@Test
	public void createTypeTest() throws TypeNotAddedException, UserNotAuthenticatedException, UserNotAddedException, UserdataNotCorrectException {
		Type type = generator.generateNewType();
		authenticate();
		provider.addType(type);
	}

	@Test(expected = TypeNotAddedException.class)
	public void noEqualTypenameTest() throws TypeNotAddedException, UserNotAuthenticatedException, UserNotAddedException, UserdataNotCorrectException {
		authenticate();
		String name = "test";
		
		Type type = new Type();
		type.setName(name);
		Type type2 = new Type();
		type2.setName(name);

		provider.addType(type);
		provider.addType(type2);
		// expect exception here
	}
	
	@Test
	public void getTypeTest() throws UserdataNotCorrectException, TypeNotAddedException, UserNotAuthenticatedException, UserNotAddedException, TypeNotFoundException {
		authenticate();
		Type type = generator.generateNewType();
		provider.addType(type);
		
		type = provider.getType(type.getName());
		Assert.assertNotNull(type);
	}
	
	@Test
	public void getTypesTest() throws UserdataNotCorrectException, TypeNotAddedException, UserNotAuthenticatedException, UserNotAddedException, TypeNotFoundException {
		authenticate();	
		List<Type> types = provider.getTypes();
		Assert.assertNotNull(types);
	}
	
	@Test
	public void updateTypeTest() throws TypeNotUpdatedException,
			UserdataNotCorrectException, TypeNotAddedException, UserNotAuthenticatedException, UserNotAddedException, TypeNotFoundException {
		authenticate();
		Type type = generator.generateNewType();
		provider.addType(type);
		
		type = provider.getType(type.getName());
		type.setName(type.getName() + "updated");
		provider.updateType(type);
	}
}
