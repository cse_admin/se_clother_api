Clother ist eine Android-basierende Beispiel-App, die im Zuge der Vorlesung Software Engineering der Otto-von-Guericke Universität Magdeburg erstellt wird. Um eine Schnittstelle zwischen der Datenbank und der zu entwicklenden App herzustellen, wurde diese API geschrieben.

Die API prüft selbstständig, ob eine neue Version verfügbar ist. Hierzu ist eine Internetverbindung notwendig. Eine Meldung über eine veraltete Version wird über Stderr ausgegeben. Darüber hinaus werden keine Transaktionen mit der Datenbank zugelassen, wenn die API nicht mehr mit dem aktuellen Datenbankschema vereinbar ist.

* Die Benutzung der API wird im WIKI erläutert.
* Bugs und fehlende Funktionalitäten sid als Issues zu formulieren

Kontakt: Michael.Lipaczewski@ovgu.de